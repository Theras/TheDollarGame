﻿using UnityEngine;

[System.Serializable]
public struct ColorSettings
{
    public Color foreground;
    public Color background;
}
