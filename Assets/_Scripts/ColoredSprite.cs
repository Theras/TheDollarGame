﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(SpriteRenderer))]
public class ColoredSprite : MonoBehaviour
{
    [SerializeField]
    private bool _foreGroundColor = false;
    [SerializeField]
    private float _animationTime = 0.3f;

    SpriteRenderer _sprite;
    ColorManager _manager;

    private void Awake()
    {
        _sprite = this.GetComponent<SpriteRenderer>();
        _manager = GameObject.FindObjectOfType<ColorManager>(); //Not a realy nice way. Should have used Zenject
        _manager.OnColorChanged += UpdateColor;

        Color color = _foreGroundColor ? _manager.CurrentColorSettings.foreground : _manager.CurrentColorSettings.background;
        _sprite.color = color;
    }

    private void OnDestroy()
    {
        _manager.OnColorChanged -= UpdateColor;
    }

    public void UpdateColor()
    {
        Sequence s = DOTween.Sequence();
        Color color = _foreGroundColor ? _manager.CurrentColorSettings.foreground : _manager.CurrentColorSettings.background;
        s.Join(DOTween.To(() => _sprite.color, x => _sprite.color = x, color, _animationTime));
        s.Play();
    }
}
