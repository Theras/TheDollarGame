﻿using DG.Tweening;
using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{
    const string PREF_LEVEL_SEED = "SEED_";
    const string PREF_LEVEL_SCORE = "LVL_SCORE_";
    const string PREF_MAX_UNLCOKED_LEVEL = "MAX_LVL";
    const int MAX_LEVEL = 18;
    const float COLOR_CHANGE_TIME = 0.3f;

    [Header("Components")]
    [SerializeField]
    Node _nodePrefab;

    [Header("References")]
    [SerializeField]
    ColorManager _colorManager;

    [Header("UI_Elements")]
    [SerializeField]
    Button _newLvl;
    [SerializeField]
    Button _deleteBtn;
    [SerializeField]
    Button _restartBtn;
    [SerializeField]
    TextMeshProUGUI _lvlDisplay;
    [SerializeField]
    TextMeshProUGUI _scoreDisplay;

    [Header("Level Selection")]
    [SerializeField]
    Transform _lvlSelection;
    [SerializeField]
    LevelSelectionButton _lvlBtnPrefab;
    List<LevelSelectionButton> _lvlBtns;



    Node[,] _gameField;
    Node[,] _copy;
    uint _width = 0;
    uint _height = 0;
    int _currentScore;

    private int _currentLvl = 0;
    public int CurrentLvl
    {
        get { return _currentLvl; }
    }
    private float _lastWon = 0;
    public Action OnLevelChanged;

    void Start ()
    {
        _lvlBtns = new List<LevelSelectionButton>();
        _currentLvl = PlayerPrefs.GetInt(PREF_MAX_UNLCOKED_LEVEL);
        _UpdateLvlDisplay();
        _newLvl.onClick.AddListener(delegate {
            PlayerPrefs.DeleteKey(PREF_LEVEL_SCORE + _currentLvl);
            _AnimateNewLevel(false);
        });
        _restartBtn.onClick.AddListener(delegate { _AnimateNewLevel(true); });
        _deleteBtn.onClick.AddListener(delegate {
            _currentLvl = 0;
            PlayerPrefs.SetInt(PREF_MAX_UNLCOKED_LEVEL,0);
            for (int i = 0; i < _lvlBtns.Count; i++)
                Destroy(_lvlBtns[i].gameObject);
            _lvlBtns.Clear();
            _UpdateLvlSelection();
            _AnimateNewLevel(false);
        });

        _UpdateLvlSelection(true);
        _CreateLevel();	
	}

    public void SetLevel(int lvl)
    {
        if (lvl != _currentLvl && lvl <= PlayerPrefs.GetInt(PREF_MAX_UNLCOKED_LEVEL))
            _currentLvl = lvl;

        _AnimateNewLevel(true);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    void _AnimateNewLevel(bool keep = false)
    {
        _currentScore = 0;
        _UpdateScoreDisplay();
        _UpdateLvlDisplay();

        Sequence changeLevel = DOTween.Sequence();
        changeLevel.Append(this.transform.DOScale(0, 0.3f));
        changeLevel.AppendCallback(delegate {
            _colorManager.ChangeColor();
            _CreateLevel(keep);
        });
        changeLevel.Append(this.transform.DOScale(1.2f, 0.3f));
        changeLevel.Append(this.transform.DOScale(1, 0.1f));
        changeLevel.Play();
    }

    void _CheckWin()
    {
        if (Time.time - _lastWon < 1)
            return;
        
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                if (_gameField[x, y].Value < 0)
                    return;
            }
        }

        //WON
        _lastWon = Time.time;

        //Update best score for this level
        int bestScore = PlayerPrefs.GetInt(PREF_LEVEL_SCORE + _currentLvl, -1);
        Debug.Log(bestScore);
        if (_currentScore < bestScore || bestScore == -1)
            PlayerPrefs.SetInt(PREF_LEVEL_SCORE + _currentLvl, _currentScore);

        //Update max unlocked level
        _currentLvl++;
        if (_currentLvl > MAX_LEVEL) _currentLvl = MAX_LEVEL;
        if(_currentLvl > PlayerPrefs.GetInt(PREF_MAX_UNLCOKED_LEVEL))
            PlayerPrefs.SetInt(PREF_MAX_UNLCOKED_LEVEL, _currentLvl);

        //Update UI
        _UpdateLvlDisplay();
        _UpdateLvlSelection();
        _AnimateNewLevel(true);
    }

    public void AddScore()
    {
        _currentScore++;
        _UpdateScoreDisplay();
    }

    private void _UpdateScoreDisplay()
    {
        int lastScore = PlayerPrefs.GetInt(PREF_LEVEL_SCORE + _currentLvl);
        if (lastScore > 0)
            _scoreDisplay.text = _currentScore + "/" + lastScore;
        else
            _scoreDisplay.text = _currentScore.ToString();
    }

    void _UpdateLvlDisplay()
    {
        if(_currentLvl >= MAX_LEVEL)
            _lvlDisplay.text = "MAX";
        else
            _lvlDisplay.text = (_currentLvl + 1).ToString();
    }

    void _CreateLevel(bool reloadLevel= false)
    {
        _currentScore = 0;
        _UpdateScoreDisplay();

        if(reloadLevel)
        {
            //Get the last Seed
            UnityEngine.Random.InitState(PlayerPrefs.GetInt(PREF_LEVEL_SEED+_currentLvl));
        }
        else
        {
            //Create new Seed
            int seed = (int)(Time.time * 100);
            UnityEngine.Random.InitState(seed);
            PlayerPrefs.SetInt(PREF_LEVEL_SEED + _currentLvl, seed);
        }

        List<Node> allNodes = new List<Node>();
        do
        {
            //Delete Old Nodes
            allNodes.Clear();
            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    Destroy(_gameField[x,y].gameObject);
                }
            }

            //Determine Width & Height depending on current LEvel
            _width = (uint)Mathf.Min(_currentLvl, MAX_LEVEL) + 2;
            _height = (uint)(_width * 9 / 16);

            //Update Camera to fit the content
            Camera.main.orthographicSize = (_height / 2.0f) + 1.5f;
            _gameField = new Node[_width, _height];

            //Create Nodes
            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    Node n = Instantiate(_nodePrefab);
                    n.Initialize();
                    n.transform.SetParent(this.transform, false);
                    n.transform.localPosition = new Vector3(x - (_width / 2.0f) + 0.5f, y - (_height / 2.0f) + 0.5f, 0);
                    n.OnValueChanged += _CheckWin;

                    _gameField[x, y] = n;
                    allNodes.Add(n);
                }
            }

            //Add random connections
            for (int x = 0; x < _width; x++)
            {
                for (int y = 0; y < _height; y++)
                {
                    List<Node> possibleNeighbors = _GetPossibleNeighbors(x, y);
                    int maxCount = possibleNeighbors.Count / 2;
                    int neighborCount = UnityEngine.Random.Range(1, maxCount + 1);

                    while (_gameField[x, y].NeighborCount < neighborCount)
                    {
                        int idx = UnityEngine.Random.Range(0, possibleNeighbors.Count);
                        _gameField[x, y].AddNeighbor(possibleNeighbors[idx]);
                    }
                }
            }
        }
        while (!_IsValidLevelStructure()); //Not all creations are valid since the connection might not be perfekt


        //Determine which node gets postive and which gets a negative values
        List<Node> positive = new List<Node>();
        List<Node> negative = new List<Node>();
        while(negative.Count < allNodes.Count / 2)
        {
            int idx = UnityEngine.Random.Range(0, allNodes.Count);
            if (!negative.Contains(allNodes[idx]))
                negative.Add(allNodes[idx]);
        }
        for(int i = 0; i< allNodes.Count; i++)
        {
            if(!negative.Contains(allNodes[i]))
                positive.Add(allNodes[i]);
        }

        //Determine the values we need to distribute to the nodes
        int points = _CaluclateGenus();
        int negativePoints = (int)(_width * _height);
        points += negativePoints; //otherwise we wont fullfill the genus
        
        //Distribut the points in small batches
        while (points > 0) 
        {
            for (int i = 0; i < positive.Count; i++)
            {
                int max = Mathf.Min(points, 3);
                int val = UnityEngine.Random.Range(0, max + 1);
                positive[i].Value += val;
                points -= val;
            }
        }

        //Distribut the negative points in small batches
        while (negativePoints > 0)
        {
            for (int i = 0; i < negative.Count; i++)
            {
                int max = Mathf.Min(negativePoints, 3);
                int val = UnityEngine.Random.Range(0, max + 1);
                negative[i].Value -= val;
                negativePoints -= val;
            }
        }

        OnLevelChanged?.Invoke();
    }

    void _UpdateLvlSelection(bool activate = false)
    {
        int maxLvl = PlayerPrefs.GetInt(PREF_MAX_UNLCOKED_LEVEL);

        //Create missing Level Selection Buttons
        if (_lvlBtns.Count <= maxLvl)
        {
            for(int i = _lvlBtns.Count; i <= maxLvl; i++)
            {
                LevelSelectionButton lb = Instantiate(_lvlBtnPrefab);
                lb.transform.SetParent(_lvlSelection, false);
                if(activate)
                    lb.Initialize(i, _currentLvl == i, this);
                else
                    lb.Initialize(i, false, this);
                _lvlBtns.Add(lb);
            }
        }
    }

    private bool _IsValidLevelStructure()
    {
        /* If we can't traverse to all nodes 
         * from the Node [0,0] it is not a 
         * valid structure                   */

        List<Node> visited = new List<Node>();
        List<Node> backLog = new List<Node>();
        Node start = _gameField[0, 0];
        backLog.Add(start);

        while(backLog.Count > 0)
        {
            Node current = backLog[0];
            backLog.RemoveAt(0);
            visited.Add(current);

            foreach (Node n in current.Neighbors)
            {
                if (visited.Contains(n))
                    continue;

                if (!backLog.Contains(n))
                    backLog.Add(n);
            }
        }
        return visited.Count == (_width * _height);
    }

    private int _CaluclateGenus()
    {
        int nodeCount = (int)(_width * _height);
        int connectionCount = 0;
        for (int x = 0; x < _width; x++)
        {
            for (int y = 0; y < _height; y++)
            {
                connectionCount += _gameField[x, y].NeighborCount;
            }
        }
        connectionCount = connectionCount / 2;
        return (connectionCount - nodeCount) + 1;
    }

    private List<Node> _GetPossibleNeighbors(int x, int y)
    {
        List<Node> nodes = new List<Node>();
        
        //Horizontal
        if (x > 0)
            nodes.Add(_gameField[x - 1, y]);
        if (x < _width - 1)
            nodes.Add(_gameField[x + 1, y]);

        //Vertical
        if (y > 0)
            nodes.Add(_gameField[x, y - 1]);
        if (y < _height - 1)
            nodes.Add(_gameField[x, y + 1]);

        //Diagonal
        if (x > 0 && y > 0)
            nodes.Add(_gameField[x - 1, y - 1]);
        if (x > 0 && y < _height - 1)
            nodes.Add(_gameField[x - 1, y + 1]);
        if (x < _width - 1 && y > 0)
            nodes.Add(_gameField[x + 1, y - 1]);
        if (x < _width - 1 && y < _height - 1)
            nodes.Add(_gameField[x + 1, y + 1]);

        return nodes;
    }
}
