﻿using UnityEngine;
using System;
using DG.Tweening;

public class ColorManager : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField]
    ColorSettings[] _colors;

    [Header("References")]
    [SerializeField]
    Material[] _foregroundMats;
    [SerializeField]
    Material[] _backgroundMats;

    int _colorIdx = 0;
    public Action OnColorChanged;


    public ColorSettings CurrentColorSettings
    {
        get { return _colors[_colorIdx]; }
    }

    private void Awake()
    {
        Camera.main.backgroundColor = _colors[_colorIdx].background;
        foreach (Material m in _foregroundMats)
            m.color = _colors[_colorIdx].foreground;
        foreach (Material m in _backgroundMats)
            m.color = _colors[_colorIdx].background;
    }

    public void ChangeColor()
    {
        _colorIdx = (_colorIdx + 1) % _colors.Length;
        Sequence s = DOTween.Sequence();
        s.Join(Camera.main.DOColor(_colors[_colorIdx].background, 0.3f));
        foreach (Material m in _foregroundMats)
            s.Join(m.DOColor(_colors[_colorIdx].foreground, 0.3f));
        foreach (Material m in _backgroundMats)
            s.Join(m.DOColor(_colors[_colorIdx].foreground, 0.3f));
        s.Play();
        OnColorChanged?.Invoke();
    }
}
