﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class LevelSelectionButton : MonoBehaviour
{
    private Button _btn;
    private int _lvl;
    private LevelManager _manager;

    [Header("Components")]
    [SerializeField]
    private ColoredGraphic _backgroundGraphic;
    [SerializeField]
    private ColoredGraphic _textGraphic;
    [SerializeField]
    private TextMeshProUGUI _text;

    List<LineManager> _connections;

    public void Initialize(int lvl, bool active, LevelManager manager)
    {
        _btn = this.GetComponent<Button>();
        _lvl = lvl;
        _text.text = (lvl+1).ToString();
        _manager = manager;
        _manager.OnLevelChanged += UpdateActive;

        _backgroundGraphic.ForeGroundColor = active;
        _textGraphic.ForeGroundColor = !active;

        _btn.onClick.AddListener(delegate
        {
            _manager.SetLevel(lvl);
        });
    }

    public void OnDestroy()
    {
        _manager.OnLevelChanged -= UpdateActive;
    }

    void UpdateActive()
    {
        bool active = _manager.CurrentLvl == _lvl;
        _backgroundGraphic.ForeGroundColor = active;
        _textGraphic.ForeGroundColor = !active;
    }
}
