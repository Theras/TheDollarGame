﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[RequireComponent(typeof(Graphic))]
public class ColoredGraphic : MonoBehaviour
{
    [SerializeField]
    private bool _foreGroundColor = false;
    public bool ForeGroundColor
    {
        get { return _foreGroundColor; }
        set { _foreGroundColor = value; UpdateColor(); }
    }

    [SerializeField]
    private float _animationTime = 0.3f;

    Graphic _graphicComponent;
    ColorManager _manager;

    private void Awake()
    {
        _graphicComponent = this.GetComponent<Graphic>();
        _manager = GameObject.FindObjectOfType<ColorManager>(); //Not a realy nice way. Should have used Zenject
        _manager.OnColorChanged += UpdateColor;

        Color color = _foreGroundColor ? _manager.CurrentColorSettings.foreground : _manager.CurrentColorSettings.background;
        _graphicComponent.color = color;
    }

    private void OnDestroy()
    {
        _manager.OnColorChanged -= UpdateColor;
    }

    public void UpdateColor()
    {
        Sequence s = DOTween.Sequence();
        Color color = _foreGroundColor ? _manager.CurrentColorSettings.foreground : _manager.CurrentColorSettings.background;
        s.Join(DOTween.To(() => _graphicComponent.color, x => _graphicComponent.color = x, color, _animationTime));
        s.Play();
    }
}
