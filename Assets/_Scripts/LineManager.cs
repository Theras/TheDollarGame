﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class LineManager : MonoBehaviour
{
    public Material m;
    public Node start;
    public Node end;
    private LineRenderer _lr;

    private void Start()
    {
        _lr = this.gameObject.AddComponent<LineRenderer>();
        _lr.material = m;
        _lr.positionCount = 2;
        _lr.startWidth = 0.1f;
        _lr.endWidth = 0.1f;
    }

    private void Update()
    {
        _lr.SetPosition(0, start.transform.position);
        _lr.SetPosition(1, end.transform.position);
    }

    public Sequence GetTakeAnimation()
    {

        Sequence lineAnimation = DOTween.Sequence();
        lineAnimation.Append(DOTween.To(() => _lr.endWidth, x => _lr.endWidth = x, 0.3f, 0.1f));
        lineAnimation.Append(DOTween.To(() => _lr.startWidth, x => _lr.startWidth = x, 0.3f, 0.1f));
        lineAnimation.Join(DOTween.To(() => _lr.endWidth, x => _lr.endWidth = x, 0.1f, 0.1f));
        lineAnimation.Append(DOTween.To(() => _lr.startWidth, x => _lr.startWidth = x, 0.1f, 0.1f));
        return lineAnimation;
    }

    public Sequence GetGiveAnimation()
    {

        Sequence lineAnimation = DOTween.Sequence();
        lineAnimation.Append(DOTween.To(() => _lr.startWidth, x => _lr.startWidth = x, 0.3f, 0.1f));
        lineAnimation.Append(DOTween.To(() => _lr.endWidth, x => _lr.endWidth = x, 0.3f, 0.1f));
        lineAnimation.Join(DOTween.To(() => _lr.startWidth, x => _lr.startWidth = x, 0.1f, 0.1f));
        lineAnimation.Append(DOTween.To(() => _lr.endWidth, x => _lr.endWidth = x, 0.1f, 0.1f));
        return lineAnimation;
    }
}
