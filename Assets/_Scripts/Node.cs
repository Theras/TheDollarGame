﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using DG.Tweening;
using System;

public class Node : MonoBehaviour, IPointerClickHandler
{
    public Action OnValueChanged;
    [SerializeField]
    TextMeshPro _textComponent;
    [SerializeField]
    SpriteRenderer _spriteComponent;
    [SerializeField]
    Material _lineMat;

    LevelManager _manager;
    InvertInputBtn _invertManager;

    List<LineManager> _connections;

    private int _value = 0;
    public int Value
    {
        get { return _value; }
        set { _value = value; _textComponent.text = _value.ToString(); }
    }

    List<Node> _neighbors;
    public int NeighborCount
    {
        get { return _neighbors.Count; }
    }
    public List<Node> Neighbors
    {
        get { return _neighbors; }
    }

    void Awake()
    {
        _connections = new List<LineManager>();
        _neighbors = new List<Node>();
        Value = 0;
    }

    void Start()
    {
        // Not a realy nice way. Should have used Zenject
        _manager = GameObject.FindObjectOfType<LevelManager>();
        _invertManager = GameObject.FindObjectOfType<InvertInputBtn>();
    }

    public void Initialize()
    {
        Value = 0;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        _manager.AddScore();
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            if (_invertManager.Inverted)
                TakeMoney();
            else
                GiveMoney();
        }
        else if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (_invertManager.Inverted)
                GiveMoney();
            else
                TakeMoney();
        }
    }

    public void TakeMoney()
    {
        Value += _neighbors.Count;
        foreach (Node n in _neighbors)
            n.Value--;

        Sequence s = DOTween.Sequence();
        foreach (LineManager lm in _connections)
        {
            s.Join(lm.GetTakeAnimation());
        }
        s.onComplete += delegate { OnValueChanged?.Invoke(); };
        s.Play();
    }

    public void GiveMoney()
    {
        Value -= _neighbors.Count;
        foreach (Node n in _neighbors)
            n.Value++;

        Sequence s = DOTween.Sequence();
        foreach(LineManager lm in _connections)
        {
            s.Join(lm.GetGiveAnimation());
        }
        s.onComplete += delegate{ OnValueChanged?.Invoke();  };
        s.Play();
    }

    public void AddNeighbor(Node other)
    {
        if (_neighbors.Contains(other))
            return;

        _neighbors.Add(other);
        other.AddNeighbor(this);

        GameObject connection = new GameObject("Connection");
        connection.transform.SetParent(this.transform, false);
        LineManager lm = connection.AddComponent<LineManager>();
        lm.start = this;
        lm.end = other;
        lm.m = _lineMat;
        this._connections.Add(lm);
    }
}
