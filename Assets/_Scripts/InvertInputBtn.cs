﻿using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class InvertInputBtn : MonoBehaviour
{
    [Header("Settings")]
    [SerializeField]
    Sprite _invertedIcon;
    [SerializeField]
    Sprite _normaIcon;

    [Header("Compoenents")]
    [SerializeField]
    Image _iconDisplay;
    Button _btn;

    List<LineManager> _connections;

    public bool Inverted { get; private set; }

    private void Awake()
    {
#if UNITY_ANDROID
        this.gameObject.SetActive(true);
#else
        this.gameObject.SetActive(false);
#endif

        _btn = this.GetComponent<Button>();
        _btn.onClick.AddListener(_ToggleBtn);
    }

    private void _ToggleBtn()
    {
        Inverted = !Inverted;
        _iconDisplay.sprite = Inverted ? _invertedIcon : _normaIcon;
        Sequence s = DOTween.Sequence();
        s.Append(this.transform.DOScale(1.2f, 0.3f));
        s.Append(this.transform.DOScale(1.0f, 0.3f));
        s.Play();
    }
}
